"""The client sending commands and returning the response in JSON."""

import json
import socket

import config as cfg
from cprint import cprint


class Client:
    def __init__(self, config):
        """Setup config for a client."""
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.ADDR = (config.SERVER, config.PORT)

    def connect(self):
        """Connect to the server."""
        self.client.connect(self.ADDR)
        self.receive()
        self.connected = True

    def idle(self):
        """Wait for a command."""
        cprint.info("[Type !help to get a list of available commands]")
        while self.connected:
            request = input("Type: ").upper()
            if request == "!DISCONNECT" or request == "!STOP":
                self.disconnect(request)
            else:
                self.send(request)
                self.receive()

    def send(self, msg):
        """Send a message to the server."""
        message = msg.encode(cfg.FORMAT)
        msg_length = len(message)
        send_length = str(msg_length).encode(cfg.FORMAT)
        send_length += b" " * (cfg.HEADER - len(send_length))
        self.client.send(send_length)
        self.client.send(message)

    def receive(self):
        """Receive a message from the server."""
        msg = self.client.recv(1024)
        received = json.loads(msg.decode(cfg.FORMAT))
        cprint.info(json.dumps(received, indent=4))
        return received

    def disconnect(self, msg):
        """Disconnect from the server, end script."""
        print("[Disconnected]")
        self.send(msg)
        self.connected = False


if __name__ == "__main__":
    client = Client(config=cfg)
    client.connect()
    client.idle()