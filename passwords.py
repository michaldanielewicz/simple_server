"""Password encryption."""

import hashlib
import os


def encrypt_password(password, salt=None):
    salt = os.urandom(32) if not salt else salt
    key = hashlib.pbkdf2_hmac(
        hash_name="sha256",
        password=password.encode('utf-8'),
        salt=salt,
        iterations=10000
    )
    return key, salt


def check_password(given_password, user_salt, user_password):
    if encrypt_password(given_password, user_salt)[0] == encrypt_password(user_password, user_salt)[0]:
        return True
    else:
        return False
