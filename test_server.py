"""Tests for server.py - Server()"""

import unittest

import config as cfg
from server import Server


class ServerTest(unittest.TestCase):

    def setUp(self):
        self.server = Server(cfg)

    def tearDown(self):
        self.server.socket.close()

    def test_start_server(self):
        self.server.start_listening(handle=False)
        self.assertTrue(self.server.server_working, "Expected server to be working.")




if __name__ == "__main__":
    unittest.main()
