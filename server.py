"""The server handling commands and returning the response in JSON."""

import json
import os
import socket
import sys
import threading
import time
from datetime import datetime

from cprint import cprint

import config as cfg


class Server:
    def __init__(self, config):
        """Setup config for a server."""
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.ADDR = (config.SERVER, config.PORT)
        self.user = User(config)
        assert self.user.load_users_data()
        self.message = Message(self.user.users_data, config)
        self.multithread = cfg.MULTITHREAD
        try:
            self.socket.bind(self.ADDR)
        except OSError:
            cprint.fatal(
                f"Wrong port/ip setting."
                "Change it in config.py."
                )
            sys.exit()

    def start_listening(self, handle=True):
        """Start server."""
        self.socket.listen()
        self.start_time = time.time()
        cprint.info(f"[Listening]: IP: {self.ADDR[0]}; PORT: {self.ADDR[1]}")
        self.server_working = True
        if handle:
            if self.multithread:
                self.accept_connections()
            else:
                self.accept_connection()

    def accept_connections(self):
        """Accept incoming connections from clients - multithread."""
        while self.server_working:
            conn, addr = self.socket.accept()
            thread = threading.Thread(
                target=self._handle_client,
                args=(conn, addr)
                )
            thread.start()
            self.connections_number = threading.activeCount() - 1
            print(f"[Active Connections]: {self.connections_number}")
        self.socket.close()

    def accept_connection(self):
        """Accept incoming connection from the client - only one client."""
        while self.server_working:
            self.conn, self.addr = self.socket.accept()
            cprint.ok(f"[Active Connection with client]: {self.addr}")
            self.connections_number = 1
            self.connected = True
            self._send_message("Connected")
            self._handle_client()
        self.socket.close()

    def _handle_client(self):
        """Handle the client."""
        while self.connected:
            msg = self._receive_message()
            print(f"[Received from {self.addr}]: {msg}")
            self._dispatcher(msg)
        self.conn.shutdown(socket.SHUT_RDWR)
        self.conn.close()
        cprint.warn(f"[Disconnected]: {self.addr}")

    def _receive_message(self):
        """Receive message."""
        msg_length = self.conn.recv(cfg.HEADER).decode(cfg.FORMAT)
        if msg_length:
            msg_length = int(msg_length)
            received_msg = self.conn.recv(msg_length).decode(cfg.FORMAT)
            return received_msg.lower()

    def _send_message(self, msg):
        """Send msg to the client."""
        msg = json.dumps(msg)
        self.conn.send(msg.encode(cfg.FORMAT))

    def prompt_user(self, msg):
        """Ask user and get answer."""
        self._send_message(msg)
        return self._receive_message()

    def _dispatcher(self, received_msg):
        """Process incoming commands."""
        if received_msg == "!disconnect":
            self.connected = False
            return
        elif received_msg == "!stop":
            self.stop()
            return
        elif received_msg == "!uptime":
            response = self.get_uptime()
        elif received_msg == "!help":
            response = self.get_help()
        elif received_msg == "!info":
            response = self.get_info()
        elif received_msg == "!login":
            username = self.prompt_user("Type your username")
            password = self.prompt_user("Type your password")
            success = self.user.log_in(username, password)
            response = "Logged in." if success else "Login failed."
        elif received_msg == "!logout":
            success = self.user.log_out()
            response = "Logged out" if success else "Not logged in"
        elif received_msg == "!add_new_user":
            new_name = self.prompt_user("Type new username") 
            new_password = self.prompt_user("Type password")
            new_pg = int(self.prompt_user(
                "Choose permission group; (0 - ADMIN), (1 - NORMAL USER)"))
            success = self.user.add_new_user(new_name, new_password,new_pg)
            response = "New user added" if success else "Adding user failed"
        elif received_msg == "!delete_user":
            delete_username = self.prompt_user("Type username to delete")
            success = self.user.delete_user(delete_username)
            response = "User deleted" if success else "Deleting user failed" 
        elif received_msg == "!priv":
            if self.user.logged:
                mail = self.prompt_user("Type message")
                recipient = self.prompt_user("Type recipient")
                success = self.message.send_mail(mail, self.user.username, recipient)
                response = "Message sent" if success else "Message sending failed"
            else:
                response = "Not logged in"
        elif received_msg == "!mailbox":
            if self.user.logged:
                response = self.message.mailbox(self.user.username)
                self._send_message(response)
                while True:
                    received_msg = self._receive_message()      
                    if received_msg == "!exit":
                        break
                    elif received_msg[:7] == "!remove":
                        # TODO: case when number not passed - regex?
                        message_id = int(received_msg[-1]) - 1     
                        if self.message.delete_message(self.user.username, message_id):
                            response = "Message removed"
                        else:
                            response = "Deleting message failed"
                        self._send_message(response)
                    else:
                        self._send_message(
                        "Type '!exit' to close mailbox. "
                        "Type '!remove <1-5>' to remove desired message."                            
                        )
                response = "Mailbox closed"
            else:
                response = "Not logged in"
        else:
            response = "No such command! Type !help for commands"
        self._send_message(response)

    def get_uptime(self):
        """Get time of server working in seconds."""
        uptime = int(time.time() - self.start_time)
        return {"uptime": f"Server is up for {uptime} seconds"}

    def get_info(self):
        """Get version of server, date of last modification."""
        modification_time = time.ctime(os.path.getmtime("server.py"))
        info = {
            "modification_time": modification_time,
            "active_connections": self.connections_number,
        }
        if self.user.permission_group == cfg.PermissionGroup.ADMIN:
            info["users"] = self.user.users_data["users"]
        elif self.user.permission_group == cfg.PermissionGroup.USER:
            info["user"] = {
                "userId": self.user.userId,
                "username": self.user.username,
                "permission_group": self.user.permission_group,
            }
        else:
            info["user"] = None
        info.update(cfg.INFO)
        return info

    def get_help(self):
        """Get list of commands and brief note."""
        return cfg.COMMANDS

    def stop(self):
        """Stop server and all connected clients."""
        cprint.warn("[Terminating server]")
        self.server_working = False
        self.connected = False


class Message():
    def __init__(self, users_data, config):
        self.users_data = users_data
        self.users_data_dir = config.USERS_DATA_DIR

    def send_mail(self, mail, sender, recipient):
        """Get private mail and save to user.json in recipient messages."""
        if recipient in list(self.users_data["users"].keys()):
            if len(self.users_data["users"][recipient]["messages"]) < 5:
                if len(mail) < 255:
                    message = {
                        "from": sender,
                        "date": datetime.now().ctime(),
                        "content": mail,
                    }
                    self.users_data["users"][recipient]["messages"].append(message)
                    if User.save_users_data(self, data=self.users_data):
                        cprint.ok("Mail saved")
                        return True
                    else:
                        cprint.err("Mail saving failed")
                else:
                    cprint.err("Mail is too long (>255)")
            else:
                cprint.err(f"Mailbox of {recipient} is full")
        else:
            cprint.err(f"No such user: {recipient}")
        return False

    def mailbox(self, username):
        """Open mailbox."""
        mailbox = self.users_data["users"][username]["messages"]
        return mailbox

    def delete_message(self, username, message_id):
        """Delete a message in username mailbox."""
        try:
            removed_msg = self.users_data["users"][username]["messages"].pop(message_id)
        except IndexError:
            removed_msg = self.users_data["users"][username]["messages"].pop()
        if User.save_users_data(self, data=self.users_data):
            cprint.ok(f"Message {removed_msg} removed")
            return True
        else:
            cprint.err("Message removing failed")
            return False


class User():
    def __init__(self, config):
        self.username = None
        self.userId = None
        self.logged = False
        self.permission_group = config.PermissionGroup.NOT_LOGGED_IN
        self.users_data_dir = config.USERS_DATA_DIR
        # TODO: add cfg.PermissionGroup as property?

    def load_users_data(self):
        """Load users.json file."""
        try:
            with open(self.users_data_dir, "r") as f:
                data = json.load(f)
        except FileNotFoundError:
            cprint.fatal("Could not open users.json")
            return False
        cprint.ok("Users data loaded.")
        self.users_data = data
        return True

    def save_users_data(self, data=None):
        """Save users_data to users.json file."""
        data = self.users_data if not data else data
        try:
            with open(self.users_data_dir, "w+") as f:
                json.dump(data, f, indent=4)
        except:
            cprint.fatal("Could not save users.json")
            return False
        cprint.ok("File saved")
        return True

    def get_users_list(self):
        """Return list of names of all not erased users."""
        return list(self.users_data["users"].keys())

    def log_in(self, username, password):
        """Login with specified username and password."""
        # TODO: add warning about full mailbox
        # if len(self.users_data["users"][self.userId]["messages"]) > 4:     
        if username in self.get_users_list():
            user = self.users_data["users"][username]
            if user["password"] == password:
                self.username = username
                self.userId = user["userId"]
                self.permission_group = user["permission_group"]
                cprint.ok(f"Authorized")
                self.logged = True
                return True
            else:
                cprint.err("Wrong password")
                return False
        cprint.err(f"No such user: {username}")
        return False

    def log_out(self):
        """Log out user."""
        if self.logged:
            self.username = None
            self.userId = None
            self.logged = False
            self.permission_group = cfg.PermissionGroup.NOT_LOGGED_IN
            cprint.ok("Username logged out")
            return True
        else:
            cprint.err("Not logged in")
            return False

    def add_new_user(self, new_user_name, new_user_password, new_user_pg):
        """Add new user and save to users.json."""
        # TODO: more stable userId method adding - gnerator?
        if self.permission_group == cfg.PermissionGroup.ADMIN:
            if new_user_name not in self.get_users_list():
                new_user_data = {
                    "userId": len(self.users_data["users"]),
                    "permission_group": new_user_pg,
                    "password": new_user_password,
                    "messages": [],
                }
                self.users_data["users"].update({new_user_name: new_user_data})
                if self.save_users_data():
                    cprint.ok(f"Added new user: {new_user_name}")
                    return True
                else:
                    cprint.err("Adding user failed")
            else:
                cprint.err(f"Username {new_user_name} already exists")
        else:
            cprint.err("Admin rights needed")
        return False

    def delete_user(self, username):
        """Delete user."""
        if self.permission_group == cfg.PermissionGroup.ADMIN:
            if username in self.get_users_list():
                self.users_data["users"].pop(username)
                if self.save_users_data():
                    cprint.ok(f"User {username} deleted")
                    return True
                else:
                    cprint.err("Deleting user failed")
            else:
                cprint.err(f"No such user {username}")
        else:
            cprint.err("Admin rights needed")
        return False


if __name__ == "__main__":
    server = Server(config=cfg)
    server.start_listening()
