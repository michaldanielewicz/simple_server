"""Configuration for server.py/client.py"""

from enum import IntEnum


PORT = 5053
SERVER = "192.168.1.8"
FORMAT = "utf-8"
HEADER = 64
MULTITHREAD = False
INFO = {"server_version": "2.0.2"}
USERS_DATA_DIR = "data/users.json"

COMMANDS = {
    "!DISCONNECT": "Disconnect client from server.",
    "!UPTIME": "Get time of server working in seconds.",
    "!INFO": "Get version of server, date of last modification etc.",
    "!HELP": "Get this list.",
    "!STOP": "Stop server and all connected clients.",
    "!LOGIN": "Log-in.",
    "!LOGOUT": "Log-out.",
    "!PRIV": "Send private message to another user.",
    "!MAILBOX": "Open mailbox, check messages or delete them.",
    "!ADD_NEW_USER": "Only for admins: add new user(s).",
    "!DELETE_USER": "Only for admis: delete user(s).",
}


class PermissionGroup(IntEnum):
    ADMIN = 0
    USER = 1
    NOT_LOGGED_IN = 2
