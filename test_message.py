"""Tests for server.py - Message()"""

import unittest
from random import randint

import config as cfg
from server import Message, User


control_number = randint(0,99)
cfg.USERS_DATA_DIR = "test_users.json"


class MessageTest(unittest.TestCase):

    def setUp(self):
        self.user = User(cfg)
        self.user.load_users_data()
        self.message = Message(self.user.users_data, cfg)

    def test_100_open_mailbox(self):
        dummy_mailbox = [{
            "from": "admin",
            "date": "Thu Jun 17 11:22:41 2021",
            "content": "test wiadomosc1"
            }]
        self.assertEqual(self.message.mailbox("test2"), dummy_mailbox)    
        
    def test_101_mail_too_long(self):
        dummy_mail = "".join([str(num) for num in range(122)])
        sender = "admin"
        recipient = "test1"
        self.assertFalse(self.message.send_mail(dummy_mail, sender, recipient))

    def test_102_mailbox_full(self):
        dummy_mail = 'test'
        sender = "admin"
        recipient = "admin"
        self.assertFalse(self.message.send_mail(dummy_mail, sender, recipient))

    def test_103_no_such_user(self):
        dummy_mail = 'test'
        sender = "admin"
        recipient = "no_such_user"
        self.assertFalse(self.message.send_mail(dummy_mail, sender, recipient))

    def test_110_send_mail(self):
        mail = f"test mail {control_number}"
        sender = "admin"
        recipient = "test1"
        success = self.message.send_mail(mail, sender, recipient)
        self.assertTrue(success, msg = "Expected to send a mail")

    def test_120_message_saved(self):
        dummy_mail = f"test mail {control_number}"
        self.assertEqual(self.message.mailbox("test1")[-1]["content"], dummy_mail)

    def test_130_message_deleted(self):
        self.message.delete_message("test1", 5)
        dummy_mail = "message no 1"
        self.assertEqual(self.message.mailbox("test1")[-1]["content"], dummy_mail)



if __name__ == "__main__":
    unittest.main()
