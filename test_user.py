"""Tests for server.py - User()"""

import unittest

import config as cfg
from server import User


cfg.USERS_DATA_DIR = "test_users.json"

class UserTest(unittest.TestCase):

    def setUp(self):
        self.user = User(cfg)

    def test_get_user_data(self):
        self.assertTrue(self.user.load_users_data(), "Expected to get users data")

    def test_check_json_data(self):
        pass
        # TODO: check if format is correct in test_users.json

    def test_get_users_list(self):
        self.user.load_users_data()
        users_list = ["admin", "test1", "test2"]
        self.assertEqual(self.user.get_users_list(), users_list)

    def test_login_err_no_such_user(self):
        self.user.load_users_data()
        self.assertFalse(self.user.log_in("wrong_password", "password"), "Expected to be failed.")

    def test_login_err_wrong_password(self):
        self.user.load_users_data()
        self.assertFalse(self.user.log_in("test1", "wrong_password"), "Expected to be failed.")

    def test_login(self):
        self.user.load_users_data()
        self.assertTrue(self.user.log_in("test1", "password"), "Expected to be log in.")

    def test_logout(self):
        self.user.load_users_data()
        self.user.log_in("test1", "password")
        self.assertTrue(self.user.log_out(), "Expected to be log out.")

    def test_add_new_user(self):
        self.user.load_users_data()
        self.user.log_in("test1", "password")
        self.assertTrue(self.user.add_new_user("test3", "password3", 0), "Expected to add new user.")

    def test_check_added_user(self):
        self.user.load_users_data()
        added_user = self.user.get_users_list()[-1]
        self.assertEqual(added_user, "test3")

    def test_delete_user(self):
        self.user.load_users_data()
        self.user.log_in("test1", "password")
        self.assertTrue(self.user.delete_user("test3"), "Expected to delete user.")


if __name__ == "__main__":
    unittest.main()
